const chai = require('chai');
const expect = chai.expect;
const is_code_applicable = require('./code_validation');
const fs = require('fs');
const path = require('path');
const customer = JSON.parse(fs.readFileSync(path.join(__dirname, './JSON/customer.json'), 'utf-8'));
const selectedOfferCode = JSON.parse(fs.readFileSync(path.join(__dirname, './JSON/offer_code.json'), 'utf-8'));
const transaction_detail = JSON.parse(fs.readFileSync(path.join(__dirname, './JSON/transaction_detail.json'), 'utf-8'));
const exist = {
    requestId: 1,
    CodeType: 'D',
    ValidFor: 'RC',
    CodeName: 'STUDENT',
    Applicable: 'Y',
    msg: ''
  } 
describe('Testing', function () {
    it('return discount detail', async function () {
        const mainFunction = await is_code_applicable(customer, selectedOfferCode, transaction_detail);
        expect(mainFunction).to.be.an('object');
        expect(exist).to.eql(mainFunction);
    })
});
